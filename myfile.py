from pyqtgraph.Qt import QtCore, QtGui, QtWidgets
import pyqtgraph.opengl as gl
import pyqtgraph as pg
import numpy as np
import random as rand
import sys
import serial
import time
from threading import Thread
from time import sleep


class Visualizer(object):
    
    
    
    
    def __init__(self):
        self.traces = dict()
        self.app = QtWidgets.QApplication(sys.argv)
        self.w = gl.GLViewWidget()
        self.w.opts['distance'] = 40 #camera distance to graph
        self.w.setWindowTitle('pyqtgraph example: GLLinePlotItem')
        self.w.setGeometry(0, 110, 1920, 1080)
        self.w.show()

        # create the background grids
        gx = gl.GLGridItem()
        gx.rotate(90, 0, 1, 0)
        gx.translate(-10, 0, 0)
        self.w.addItem(gx)
        gy = gl.GLGridItem()
        gy.rotate(90, 1, 0, 0)
        gy.translate(0, -10, 0)
        self.w.addItem(gy)
        gz = gl.GLGridItem()
        gz.translate(0, 0, -10)
        self.w.addItem(gz)

        self.n = 50
        self.m = 1000
        self.y = np.linspace(-10, 10, self.n)
        self.x = np.linspace(-10, 10, self.m)
        self.zeros = np.array([np.zeros(self.m)]*self.n)
        self.desired = np.array([np.zeros(self.m)]*self.n)
        self.desired2 = np.array([np.zeros(self.m)]*self.n)
        self.matrix = np.array([np.zeros(self.m)]*self.n)
        self.phase = .001
        self.change = 0

        self.arduino = serial.Serial(port='/dev/rfcomm0', baudrate=115200, timeout=.1)

        def random_matrix(x, y): #Used for testing
            s = (y,x)
            matrise = np.zeros(s)
            for i in range(x):
                for j in range(y):
                    matrise[j][i] = rand.randint(0, 10)
            return matrise

        
        
        def matrix_spread(matrix): #Does not create a natural wave sadly. Deprecated
            copy = np.array([np.zeros(self.m)]*self.n)
            copy[:] = matrix[:]
            for i in range(len(matrix)):
                for j in range(len(matrix[i])):
                    if(matrix[i][j] == 0):
                        continue
                    for y in range(-4, 5, 1):
                        if(i+y > 49):
                            break
                        for x in range(-80, 100, 1):
                            x2 = x
                            y2 = y
                            if(j+x > 999):
                                break
                            if(y2 == 0):
                                y2 = 0.1
                            if(x2==0):
                                x2 = 0.1
                            spread = matrix[i][j]*abs(1/y2)*abs(1000/(x2))*0.01
                            print(matrix[i][j])
                            copy[i+y][j+x] = spread
            return copy
        
        
        
        
        


            
        
        for i in range(self.n): #start position?
            yi = np.array([self.y[i]] * self.m)
            z = self.matrix[i]
            pts = np.vstack([self.x, yi, z]).transpose()
            self.traces[i] = gl.GLLinePlotItem(pos=pts, color=pg.glColor(
                (i, self.n * 1.3)), width=(i + 1) / 10, antialias=True)
            self.w.addItem(self.traces[i])
    
    def write_read(self):
            #self.arduino.write(bytes(x, 'utf-8'))
            time.sleep(0.05)
            data = self.arduino.readline()
            return data

    def plot_wave(self, matrix, x, y, height):
            x -= 500
            y -= 25
            wave_matrix = np.loadtxt('wave_2.txt')
            wave_matrix = wave_matrix * height
            wave_matrix = np.roll(wave_matrix, x, axis = 1)
            if(int(x) != 0):
                for i in range(0, x, int(x/abs(x))):
                    wave_matrix[:,i] = 0
            wave_matrix = np.roll(wave_matrix, y, axis = 0)
            if(int(y) != 0):    
                for i in range(0, y, int(y/abs(y))):
                    wave_matrix[i,:] = 0
            out = np.add(matrix, wave_matrix)
            return out
    
    def plot_wave_top(self, matrix, x, y, height):
            x -= 500
            y -= 25
            wave_matrix = np.loadtxt('wave_2.txt')
            wave_matrix = wave_matrix * height
            wave_matrix = np.roll(wave_matrix, x, axis = 1)
            if(int(x) != 0):
                for i in range(0, x, int(x/abs(x))):
                    wave_matrix[:,i] = 0
            wave_matrix = np.roll(wave_matrix, y, axis = 0)
            if(int(y) != 0):    
                for i in range(0, y, int(y/abs(y))):
                    wave_matrix[i,:] = 0
            for i in range(self.n):
                for j in range(self.m):
                    if(matrix[i][j] > wave_matrix[i][j]):
                        wave_matrix[i][j] = matrix[i][j]

            out = wave_matrix
            return out
    
    def start(self):
        if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
            QtWidgets.QApplication.instance().exec()

    def set_plotdata(self, name, points, color, width):
        self.traces[name].setData(pos=points, color=color, width=width)
    
    

    def update(self):
        for i in range(self.n): #each i is a 2D function
            yi = np.array([self.y[i]] * self.m)
            for j in range(self.m):
                differance = self.desired[i][j]-self.matrix[i][j]
                self.matrix[i][j] += differance*(self.phase) 
            pts = np.vstack([self.x, yi, self.matrix[i]]).transpose()
            self.set_plotdata(
                name=i, points=pts,
                color=pg.glColor((i, self.n * 1.3)),
                width=(i + 1) / 10
            )


        if(self.change > 1):
            z = int(self.write_read().decode())
            print("before",z)
            x = int(input("x coordinate"))
            y = int(input ("y coordinate"))
            z = z/60
            print("after", z)
            #self.desired = self.zeros
            self.desired = self.plot_wave_top(self.desired, x, y, z)
            self.change = 0
            self.phase = 0
            
        self.change += 0.01
        self.phase += 0.002 #How fast the graph falls/moves, it probably determines the speed of change in the phase

    def animation(self):
        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(10) #frames per second??
        self.start()


# Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    v = Visualizer()
    v.animation()