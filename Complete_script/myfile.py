from pyqtgraph.Qt import QtCore, QtGui, QtWidgets
import pyqtgraph.opengl as gl
import pyqtgraph as pg
import numpy as np
import random as rand
import sys


class Visualizer(object):
    def __init__(self):
        #Setting up coordinate system variables such as camera
        self.traces = dict()
        self.app = QtWidgets.QApplication(sys.argv)
        self.w = gl.GLViewWidget()
        self.w.opts['distance'] = 40 #camera distance to graph
        self.w.setWindowTitle('pyqtgraph example: GLLinePlotItem')
        self.w.setGeometry(0, 110, 1920, 1080)
        self.w.show()

        # create the background grids
        gx = gl.GLGridItem()
        gx.rotate(90, 0, 1, 0)
        gx.translate(-10, 0, 0)
        self.w.addItem(gx)
        gy = gl.GLGridItem()
        gy.rotate(90, 1, 0, 0)
        gy.translate(0, -10, 0)
        self.w.addItem(gy)
        gz = gl.GLGridItem()
        gz.translate(0, 0, -10)
        self.w.addItem(gz)

        self.n = 50 #Number of points in y direction
        self.m = 1000 #Number of points in x direction
        self.y = np.linspace(-10, 10, self.n)
        self.x = np.linspace(-10, 10, self.m)
        self.desired = np.array([np.zeros(self.m)]*self.n) #Matrix that the coordinate system is moving towards
        self.matrix = np.array([np.zeros(self.m)]*self.n) #Matrix that the coordinate system is currently in
        self.phase = 0 #Determines the speed at which the matrix moves from self.matrix to self.desired
        self.desired2 = np.array([np.zeros(self.m)]*self.n)
        self.bool = 0

        def random_matrix(x, y): #Used for testing
            s = (y,x)
            matrise = np.zeros(s)
            for i in range(x):
                for j in range(y):
                    matrise[j][i] = rand.randint(0, 10)
            return matrise
        
        def matrix_spread(matrix): #Does not create a natural wave sadly. #Not used
            copy = np.array([np.zeros(self.m)]*self.n)
            copy[:] = matrix[:]
            for i in range(len(matrix)):
                for j in range(len(matrix[i])):
                    if(matrix[i][j] == 0):
                        continue
                    for y in range(-4, 5, 1):
                        if(i+y > 49):
                            break
                        for x in range(-80, 100, 1):
                            x2 = x
                            y2 = y
                            if(j+x > 999):
                                break
                            if(y2 == 0):
                                y2 = 0.1
                            if(x2==0):
                                x2 = 0.1
                            spread = matrix[i][j]*abs(1/y2)*abs(1000/(x2))*0.01
                            print(matrix[i][j])
                            copy[i+y][j+x] = spread
            return copy
        
        
        def plot_wave(matrix, x, y, height): #plots a wave in a matrix using a matrix stored in a txt file
            x -= 500
            y -= 25
            wave_matrix = np.loadtxt('wave_2.txt')
            wave_matrix = wave_matrix * height
            wave_matrix = np.roll(wave_matrix, x, axis = 1)
            if(int(x) != 0):
                for i in range(0, x, int(x/abs(x))):
                    wave_matrix[:,i] = 0
            wave_matrix = np.roll(wave_matrix, y, axis = 0)
            if(int(y) != 0):    
                for i in range(0, y, int(y/abs(y))):
                    wave_matrix[i,:] = 0
            out = np.add(matrix, wave_matrix)
            return out
        self.desired = plot_wave(self.desired, 500, 45, 0.5)
        self.desired = plot_wave(self.desired, 500, 25, 1.1)
        self.desired = plot_wave(self.desired, 300, 25, -0.3)
        
        self.desired2 = plot_wave(self.desired2, 740, 10, 0.5)
        self.desired2 = plot_wave(self.desired2, 790, 30, 1.1)
        self.desired2 = plot_wave(self.desired2, 200, 33, 0.2)

        self.desired3 = plot_wave(self.desired, 500, 45, 0.5)
        self.desired3 = plot_wave(self.desired, 500, 25, 1.1)
        self.desired3 = plot_wave(self.desired, 300, 25, -0.3)


            
        
        for i in range(self.n): #start position?
            yi = np.array([self.y[i]] * self.m)
            z = self.matrix[i]
            pts = np.vstack([self.x, yi, z]).transpose()
            self.traces[i] = gl.GLLinePlotItem(pos=pts, color=pg.glColor(
                (i, self.n * 1.3)), width=(i + 1) / 10, antialias=True)
            self.w.addItem(self.traces[i])

    def start(self):
        if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
            QtWidgets.QApplication.instance().exec()

    def set_plotdata(self, name, points, color, width):
        self.traces[name].setData(pos=points, color=color, width=width)

    def update(self):
        for i in range(self.n): #each i is a 2D function
            yi = np.array([self.y[i]] * self.m)
            for j in range(self.m):
                differance = self.desired[i][j]-self.matrix[i][j]
                self.matrix[i][j] += differance*self.phase
            pts = np.vstack([self.x, yi, self.matrix[i]]).transpose()
            self.set_plotdata(
                name=i, points=pts,
                color=pg.glColor((i, self.n * 1.3)),
                width=(i + 1) / 10
            )
            self.phase += .00001 #How fast the graph falls/moves, it probably determines the speed of change in the phase
            if(self.phase == 0.1):
                self.phase = 0
                if(self.bool == 1):
                    self.desired = self.desired2
                    self.bool = 0
                else:
                    self.bool = 1
                    self.desired = self.desired3
            

    def animation(self):
        timer = QtCore.QTimer()
        timer.timeout.connect(self.update)
        timer.start(10) #frames per second??
        self.start()


# Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    v = Visualizer()
    v.animation()